package main;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Random;

public class Baralho {
	static String[] ranks = {"As", "Dois", "Tres", "Quatro", "Cinco", "Seis", "Sete", "Oito", "Nove", "Dez", "Dama", "Valete", "Rei"};
	static String[] naipes = {"Ouros", "Espadas", "Copas", "Paus"};

	private ArrayList<Carta> cartas = new ArrayList<>();

	public Baralho() {
		for(int i = 0; i < ranks.length; i++) {
			for(int j = 0; j < naipes.length; j++) {
				cartas.add(new Carta(ranks[i], naipes[j]));

			}
		}
	}

	private Random random = new Random();

	public Carta sortear() {

		int sorteada = random.nextInt(cartas.size());
		Carta cartaSorteada = cartas.get(sorteada);
		cartas.remove(sorteada);

		return cartaSorteada;		
	}
}