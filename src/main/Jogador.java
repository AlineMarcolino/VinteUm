package main;

import java.util.Scanner;

public class Jogador {
	
	int pontos = 0;

	public void jogar() {
		Baralho baralho = new Baralho();
		Scanner scanner = new Scanner(System.in);
		
		System.out.println("Olá Jogador, Bem vindo a mesa!");
		System.out.println("Digite S para iniciar as jogadas!");
		String entrada =  scanner.next();

		while (pontos < 21 && entrada.toUpperCase().equals("S")) {

			Carta carta = baralho.sortear();
			int ponto = carta.getValor();
			System.out.println("Carta sorteada: " + carta + " que vale " + ponto + " pontos!");
			pontos += ponto;	
			System.out.println("Digite S para proxima carta!");
			entrada = scanner.next();
		}
		System.out.println("Você teve: " + pontos + " pontos!");
	}
}