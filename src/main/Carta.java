package main;

public class Carta {
	public String rank;
	public String naipe;
	
	Carta(String rank, String naipe ){
		this.rank = rank;
		this.naipe = naipe;
	}
	
	public int getValor() {
		switch (rank) {
		case "As": return 1;
		case "Dois": return 2;
		case "Tres": return 3;
		case "Quatro": return 4;
		case "Cinco": return 5;
		case "Seis": return 6;
		case "Sete": return 7;
		case "Oito": return 8;
		case "Nove": return 9;
		case "Dez":
		case "Dama":
		case "Valete":
		case "Rei" :
			return 10;
		}
		return 0;
	}
	
	public String toString() {
		return rank + " de " + naipe;
	}
}
